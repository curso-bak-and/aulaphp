<?php

$nome = 'Rener';
$idade = 22;
$email = 'rener@outlook.com';
$senha = '12345678';
$curso = ['PHP', 'HTML', 'CSS'];


echo '<h1> Trabalhando com Estrutura condicional,/h1>';

echo '<h2>Exemplo de if (se...)</h2>';

if ($idade >= 18) {
    echo "O usuário $nome é maior de idade";
}

echo "<hr>";

######################################
echo "<h2>Exemplo de if e else</h2>";

echo ($idade >=18)? "Maior de Idade" : "Menor de Idade";



#######################################
echo "<hr>";
echo "<h2>Exemplo de if e else</h2>";

if ($email == 'rener@outlook.com' && $senha == '12345678') {
        echo "Usuário Logado";
}else{
    echo "Usuário ou Senha Inválida";
}

#######################################
echo "<hr>";
echo "<h2>Exemplo de if e else</h2>";
if ($email == 'rener@outlook.com') {
    if($senha == '12345678') {
        echo "Usuário Logado";
    }else{
        echo "Usuário ou Senaha invalida";
    }
}else{
    echo "Usuário ou Senha Inválida";
}

#######################################
echo "<hr>";
echo "<h2>Exemplo de Multiplas Condições</h2>";

$num1 = 10;
$num2 = 20;

if($num1 == $num2){
    echo "os números são iguais";
}elseif ($num1 > $num2){
    echo "O número 1 é maior que o número 2";
}else{
    echo "O número 2 é maior que o número 1";
}

#######################################
echo "<hr>";
echo "<h2>Exemplo de GET</h2>";

$menu = $_GET['menu']?? "Home";

switch(strtolower($menu)) {
     case "Home";
         echo "Página Principal"; 
         break;
     case "Contato";
         echo "Página Contato";
         break;
     default:
         echo "Página Erro 404";
}
?>