<?php

$lista_compra = ['arroz', 'feijão', 'banana', 'detergente', 'sabonete'];

$lista_compra[''] = 'Reação PET';

echo "<pre>";
print_r($lista_compra);
echo "</pre>";

echo "<hr>";

var_dump($lista_compra);

echo "<hr>";


echo $lista_compra[4]. ", " .$lista_compra[2];

foreach ($lista_compra as $item) {
    echo $item. ", ";
    echo !"<br>";
}    

echo "<hr>";

#Array Associativo

$funcionario = [ 
      "nome" => "Rener Augusto", 
      "cargo" => "MEP",
      "idade" => 22,
      "saláruio" => 1500.50,
      "ativo" => true
];

var_dump($funcionario);

echo $funcionario['cargo'];

#Array multidimensional

$funcionarios= [ 
    [
        "nome" => "Rener Augusto", 
        "cargo" => "MEP",
        "idade" => 22,
        "saláruio" => 1500.5,
        "ativo" => true,
        "cursos" => ["Web", "PHP", "Javascript"] 
    ],

    [
    
        "nome" => "Irineu",   
        "cargo" => "MEP",       
        "idade" => 30, 
        "saláruio" => 2000,       
        "ativo" => true,
        "crusos" => []                  
    ],

    [
    
        "nome" => "lurdinha",   
        "cargo" => "MEP",       
        "idade" => 50, 
        "saláruio" => 1500,       
        "ativo" => true,
        "cursos" => ["Photoshop", "illustrator"]                   
    ],              
];

var_dump($funcionarios[1]);
 

echo 'nome:', $funcionarios[2]['nome'];

echo "<br>";

echo 'cargo:', $funcionarios[2]['cargo'];

echo "<br>";

echo 'crusos:', $funcionarios[2]['cursos'][0];

echo "<hr>";

foreach ($funcionarios as $item) {

    echo 'nome:'.$item["nome"]; 
    echo "<br>";
    
    echo 'cargo:'.$item["cargo"];
    
    echo "<br>";
    
    echo 'idade:'.$item["idade"];
     
    echo "<br>";

    echo "cursos:".implode(", ", $item ['cruso']);


    echo "<hr>";
    
}


