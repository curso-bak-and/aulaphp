<?php
    $nome = 'Rener Augusto';
    $nascionalidade = 'brasileiro';
    $proficao = 'serralheiro';
    $cpf = ' 480.170.178.xx';
    $rg= '57.009.851‐X';
    $cnpj = '51.757.388/0001‐00';
    $cep = '17512-855';

?>


<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Declaração de Local de Trabalho</title>
</head>
<body>
    <h1>Declaração de Local de Trabalho</h1>

    <p>
    Eu, <?=$nome ?>, <?=$nascionalidade ?>, <?=$proficao ?>, inscrito(a)no CPF sob o <?=$cpf ?> e no RG <?=$rg ?>, declaro paraosdevidos fins que possuo vínculo empregatício com a empresa Senac inscrita no CNPJ sob o nº  <?=$cnpj ?>, localizada à R.Paraíba,  125 ‐ Marília, SP, <?=$cep ?>.
    </p>

    <p>
    Por ser expressão da verdade,firmo a presente para efeitos legais.
    </p>

    <p>
    Marília–SP, 15 de Setembrode 2022
    </p>


    <p>
      <?=$nome ?>
    </p>

</body>
</html>






  