<?php

function soma($num1, $num2)
{
    $total = $num1 + $num2;

    return $total;
}

function multiplicacao($num3, $num4)
{
    $total = $num3 * $num4;

    return $total;
}

function divisao($num5, $num6)
{
    $total = $num5 / $num6;

    return $total;
}

function subitracao($num7, $num8)
{
    $total = $num7 - $num8;

    return $total;
}


echo soma(10, 5);
echo "<hr>";
echo subitracao(10, 3);
echo "<hr>";
echo multiplicacao(10, 9);
echo "<hr>";
echo divisao(10, 7);
echo "<hr>";


echo par_ou_impar(10);

function par_ou_impar($numero){

    if($numero % 2 == 0){
        return "Número é Par";
    }
    return "Número é impar";

}

function par_ou_impar_mod2($numero){
     return ($numero % 2 == 0)? " Número é Par" : "Número é impar";

}

///////////////////////////////

echo "<hr";

function geradoSenhaComFor($senhaInicial, $senhaFinal){

    for($contador = $senhaInicial; $contador <= $senhaFinal; $contador++){
        echo $contador. "-";
    }
}

geradoSenhaComFor(10,20);



echo "<hr";

function geradoSenhaComWhile($senhaInicial = 1, $senhaFinal = 10){

    $contador = $senhaInicial;

    While( $contador <= $senhaFinal){
        echo $contador. "-";
        $contador++;
    }
}

geradoSenhaComWhile(10,20);


